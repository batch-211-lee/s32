let http = require("http");

let port = 4000

let server = http.createServer(function(request,response){
	if (request.url == "/"){
		response.writeHead(200, {"Content-Type" : "text/plain"})
		response.end("Welcome to Booking System");
	}
	if (request.url == "/profile"){
		response.writeHead(200, {"Content-Type" : "text/plain"})
		response.end("Welcome to your profile!")
	}
	if (request.url == "/courses"){
		response.writeHead(200, {"Content-Type" : "text/plain"})
		response.end("Here's our courses available");
	}
	if (request.url == "/addcourse"){
		response.writeHead(200, {"Content-Type" : "text/plain"})
		response.end("Add a course to our resources");
	}
	if (request.url == "/updatecourse"){
		response.writeHead(200, {"Content-Type" : "text/plain"})
		response.end("Update a course in our resources");
	}
	if (request.url == "/archivecourse"){
		response.writeHead(200, {"Content-Type" : "text/plain"})
		response.end("Archive a course in our resources");
	}
});

server.listen(port);